# configurator_git #

Скрипт сконфигурирует Git с базовыми настройками под вашу ОС и текущего пользователя

### Как использовать? ###

* Запустить скрипт start_<Ваша ОС>.sh
* Будет создан файл .env
* В файле .env добавить свой username из "Bitbucket -> Personal settings -> Account settings -> Bitbucket profile settings" в переменную GIT_USER_NAME
* В файле .env добавить свой email из "Bitbucket -> Personal settings -> Email aliases -> Your Atlassian account address is" в переменную GIT_USER_EMAIL
* Запустить скрипт start_<Ваша ОС>.sh еще раз
