#!/bin/bash

if ! [ -f ./.env ]
then
    cp -f ./.example.env ./.env
fi

sed -i 's/\r$//g' ./.env
export $(cat ./.env)

if [ "$GIT_USER_NAME" == "" ] || [ "$GIT_USER_EMAIL" == "" ]
then
    echo -e "\n\n\n\n\nВнимание"
    echo "Нужно заполнить переменные окружения Git в файле $(pwd)/.env"
    exit 1
fi

git config --global user.name "$GIT_USER_NAME"
git config --global user.email "$GIT_USER_EMAIL"
git config --global core.autocrlf input
git config --global core.safecrlf warn
git config --global core.quotepath off
git config --global color.ui auto

if [ -f ~/.gitconfig ]
then
    echo -e "\n\n\n\n\n"
    echo -e "[ OK ] Конфигурация Git завершена успешно\n"
    git config --global --list
else
    echo -e "\n\n\n\n\n"
    echo -e "[FAIL] Конфигурация Git завершена с ошибкой\n"
    echo -e "Настройте Git для локальной учетной записи ($USER) другим способом\n"
    exit 1
fi
